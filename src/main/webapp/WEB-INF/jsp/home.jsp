<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 5/3/13
  Time: 8:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>2ndStack Java Spring & ExtJS Training</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/extjs/resources/css/ext-all.css'/>">

    <script type="text/javascript">

        <%-- fungsi ini untuk menghapus bagian jsessionid pada url --%>
        var cleanUrl = function (path) {
            var indexOfJSessionId = path.indexOf(";jsessionid");
            if (indexOfJSessionId != -1) {
                path = path.substring(0, indexOfJSessionId);
            }
            return path;
        };

        <%-- url default untuk student --%>
        var studentApiUrl = cleanUrl('<c:url value="/api/student"/>');

    </script>

    <script type="text/javascript" src="<c:url value='/resources/extjs/ext-all-dev.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/app/app.js'/>"></script>

</head>
<body>
<h1>HOME</h1>
</body>
</html>