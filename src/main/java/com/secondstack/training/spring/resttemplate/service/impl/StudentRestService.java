package com.secondstack.training.spring.resttemplate.service.impl;

import com.secondstack.training.spring.resttemplate.domain.Student;
import com.secondstack.training.spring.resttemplate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/7/13
 * Time: 10:15 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class StudentRestService implements StudentService {

    /**
     * Url dasar untuk API Student di server
     */
    public final static String STUDENT_URL = "http://localhost:8080/api/student";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void save(Student student) {
        restTemplate.postForEntity(STUDENT_URL, student, String.class);
    }

    @Override
    public Student update(Integer id, Student student) {
        restTemplate.put(STUDENT_URL + "?id=" + id, student);
        return findById(id);
    }

    @Override
    public void delete(Student student) {
        delete(student.getId());
    }

    @Override
    public void delete(Integer id) {
        restTemplate.delete(STUDENT_URL + "?id=" + id);
    }

    @Override
    public List<Student> findAll() {
        Student []students = restTemplate.getForObject(STUDENT_URL, Student[].class);
        return Arrays.asList(students);
    }

    @Override
    public Student findById(Integer id) {
        return restTemplate.getForObject(STUDENT_URL + "?id=" + id, Student.class);
    }
}
