package com.secondstack.training.spring.resttemplate.domain.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Sex {
     MALE, FEMALE
}
