package com.secondstack.training.spring.resttemplate.controller.api;

import com.secondstack.training.spring.resttemplate.domain.Student;
import com.secondstack.training.spring.resttemplate.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/student")
public class StudentApiController {

    protected static Logger logger = Logger.getLogger("controller");

    /**
     * Class yang menangani segala proses ke server REST API.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Menangani request membuat data Student baru.
     * @param student
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Student student){
        logger.debug("Received rest request to create student");
        studentService.save(student);
    }

    /**
     * Menangani request mengedit data Student.
     * @param id
     * @param student
     */
    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Student student){
        logger.debug("Received rest request to update student");
        studentService.update(id, student);
    }

    /**
     * Dapatkan semua data Student.
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Student> findAll(){
        logger.debug("Received rest request to get list student");
        List<Student> studentList = studentService.findAll();
        return studentList;
    }

    /**
     * Dapatkan Student berdasarkan id
     * @param id
     * @return
     */
    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Student findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data student");
        Student student = studentService.findById(id);
        return student;
    }

    /**
     * Hapus Student dengan id tertentu
     * @param id
     */
    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete student");
        studentService.delete(id);
    }

}
